Funcion es_bisiesto <- bisiesto (anio)
	definir es_bisiesto, divisible100, divisible4, divisible400 Como Logico;
	
	divisible4 <- anio mod 4 = 0;
	divisible100 <- anio mod 100 = 0;
	divisible400 <- anio mod 400 = 0;
	es_bisiesto <- (divisible4 y !divisible100) o divisible400;
FinFuncion

Proceso dias_meses
	Definir mes, anio Como Entero;
	Escribir "Ingrese el n�mero de mes";
	leer mes;
	Segun mes Hacer
		1,3,5,7,8,10,12:
			Escribir "31 d�as";
		
		2: 
			Escribir "Introduzca A�o";
			leer anio;
			si bisiesto(anio) entonces 
				escribir "29 d�as";
			sino 
				escribir "28 d�as";
			FinSi
		4,6,9:
			Escribir "30 d�as";
		De Otro Modo:
			Escribir "Mes Inexistente";
	FinSegun

FinProceso
