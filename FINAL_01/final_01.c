#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define PI 3.14159265



int main()
{
    double numero, seno, coseno, tangente;
    numero=0;
    printf("Ingrese un numero:");
    scanf ("%lf",&numero);
    printf("%lf\n", numero);
    seno= sin(numero*PI/180);
    printf("El resultado del seno es: %.3lf\n" , seno);
    coseno= cos(numero*PI/180);
    printf("El resultado del coseno es: %.3lf\n" , coseno);
    tangente= tan(numero*PI/180);
    printf("El resultado de la tangente es: %.3lf\n" , tangente);

    return 0;
}