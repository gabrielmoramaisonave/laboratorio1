Proceso cual_es_menor
	definir a, b como entero;
	a<-0;
	b<-0;
	Escribir "Ingrese el primer valor";
	leer a;
	Escribir "Ingrese el segundo valor";
	leer b;
	Si a<b Entonces
		Escribir a , " es menor que ", b;
	SiNo
		Escribir b , " es menor que ", a;
	FinSi
FinProceso
