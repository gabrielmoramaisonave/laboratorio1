Proceso sumatoria_primeros_10_enteros
	Definir n, suma Como Entero;
	suma<-0;
	n<-0;
	Repetir
		n<-n+5;
		suma<- suma+n;
		Escribir n;
	Hasta Que n=100;
	Escribir "La suma de los primeros 100 n�meros m�ltiplos de 5 es: ", suma;
FinProceso
