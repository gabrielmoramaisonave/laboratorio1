Proceso sumatoria_numeros_pares_hasta_100
		
		Definir n, suma Como Entero;
		
		suma<-0;
		
		n<-0;
		
		Repetir
			n<-n+2;
			
			suma<- suma+n;
			
			Escribir n;
		Hasta Que n=100;
		Escribir "La suma de los primeros 100 n�meros m�ltiplos de 2 es: ", suma;
FinProceso


