Proceso calculo_hipotenusa
	definir a,b,h Como Real;
	a<-0;
	b<-0;
	h<-0;
	Escribir "Introduzca los valores de los catetos";
	leer a, b;
	h<- rc((a^2)+(b^2));
	Escribir "El valor de la Hipotenusa es ", h;
FinProceso
