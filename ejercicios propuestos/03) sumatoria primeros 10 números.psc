Proceso sumatoria_primeros_10_enteros
	Definir n, suma Como Entero;
	suma<-0;
	n<-0;
	Repetir
		n<-n+1;
		suma<- suma+n;
	Hasta Que n=10;
	Escribir "La suma de los primeros 10 n�meros enteros es: ", suma;
FinProceso
