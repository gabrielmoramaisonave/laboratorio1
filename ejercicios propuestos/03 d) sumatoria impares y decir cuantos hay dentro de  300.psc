Proceso sumatoria_numeros_pares_hasta_100
		
		Definir n, suma, imp, i, acum Como Entero;
		
		suma<-0;
		imp<-0;
		n<-0;
		i<-0;
		acum<-0;
		
		Repetir
			n<-n+1;
			Si n MOD 2 = 1 Entonces
				imp<- imp+1;
			FinSi
		Hasta Que n=300;
	
		Escribir "Dentro de los primeros 300 n�meros enteros hay ", imp, " n�meros impares";
	
		Para i<- 1 Hasta 300 Con Paso 1 Hacer
			Si i MOD 2 = 1 Entonces
				suma <- suma+i;
				acum<-suma;
			FinSi
		FinPara
		Escribir "La suma de los n�meros impares dentro de los primeros 300 n�meros enteros es ", acum;
FinProceso


