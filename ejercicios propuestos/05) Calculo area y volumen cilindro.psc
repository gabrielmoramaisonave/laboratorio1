Proceso calculo_area_y_volumen_cilindro
	Definir r, h, volumen, area Como Real;
	r<-0;
	h<-0;
	volumen<-0;
	area<-0;
	Escribir "Ingrese los valores del Radio y la Altura del cilindro";
	leer r,h;
	volumen<- PI*(r^2)*h;
	area<- (2*PI*r*h)+(2*PI*(r^2));
	Escribir "El volumen del cilindro es ", volumen;
	Escribir "";
	Escribir "El area del cilindro es ", area;
	
FinProceso
