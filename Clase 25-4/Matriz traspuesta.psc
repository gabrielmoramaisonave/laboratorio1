Proceso matriz_traspuesta
	Definir iLong,jlong,i,j Como entero;
	definir matriz Como Caracter;
	dimension matriz[3,3];
	i<-0;
	j<-0;
	iLong<-3;
	jlong<-3;
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Escribir "_______";
		para j<-0 hasta 2 con paso 1 Hacer
			Escribir "Ingresar Elemento [",i,",",j,"] de la Matriz";
			leer matriz[i,j];
		FinPara
	FinPara
	
	Escribir "Matriz Original";
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Escribir "_______";
		para j<-0 hasta 2 con paso 1 Hacer
			Escribir Sin Saltar Matriz[i,j], " ";
		FinPara
	FinPara
	
	Escribir "Matriz Traspuesta";
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Escribir "_______";
		para j<-0 hasta 2 con paso 1 Hacer
			Escribir Sin Saltar Matriz[j,i], " ";
		FinPara
	FinPara
	
	
FinProceso
