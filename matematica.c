#include <stdio.h>
#include <math.h>


int main (){ 
    // PI
    printf ("PI: %f\n", M_PI);
    printf ("PI/2: %f\n", M_PI_2);
    printf ("PI/4: %f\n", M_PI_4);
    printf ("1/PI: %f\n", M_1_PI);

    //E
    printf ("E: %f\n", M_E);

    // Raices
    printf ("Raiz 2: %f\n", sqrt(2));

    // Potencia 
    printf ("2^4: %d\n", (int)pow(2,4));
return 0;
}